package service;

import model.Discount;
import model.Product;
import model.enums.DiscountType;

import java.math.BigDecimal;

public class PriceCalculator {
    public BigDecimal calculatePrice(Product product, double quantity) throws  IllegalArgumentException {
        BigDecimal result = BigDecimal.ZERO;
        switch (product.getUnit()) {
            case PACKAGE:
                if (quantity % product.getPackageSize() != 0) {
                    throw new IllegalArgumentException("Package size incorrect");
                }
                result = product.getPrice().multiply(BigDecimal.valueOf(quantity / product.getPackageSize()));
                break;
            case UNIT:
            default:
                result = product.getPrice().multiply(BigDecimal.valueOf(quantity));
                break;
        }

        if ((product.getApplicableDiscounts() != null) &&(product.getApplicableDiscounts().size() > 0)) {
            final Discount discount = product.getApplicableDiscounts().get(0);
            if (discount.getType() == DiscountType.UNITPRICE) {
                result = result.multiply(BigDecimal.valueOf((double) ( 100 - discount.getDiscountPourcentage()) / 100));
            }
            if (discount.getType() == DiscountType.PACKAGEPRICE) {
                int numberOfPackages = (int) (quantity / discount.getDiscountedPackageSize());
                BigDecimal substractAmout = product.getPrice().multiply(BigDecimal.valueOf((double) discount.getDiscountPourcentage() / 100)).multiply(BigDecimal.valueOf(discount.getDiscountedPackageSize())).multiply(BigDecimal.valueOf(numberOfPackages));
                result = result.subtract(substractAmout);
            }
        }

        return result;
    }
}
