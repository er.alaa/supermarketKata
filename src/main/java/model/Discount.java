package model;

import lombok.Builder;
import lombok.Data;
import model.enums.DiscountType;

@Data
@Builder
public class Discount {
    private Long id;
    private DiscountType type;
    private Integer discountPourcentage;
    private Integer discountedPackageSize;
}
