package model;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import model.enums.PriceUnit;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class Product {

    private Long id;
    private String name;
    private BigDecimal price;
    @Builder.Default private PriceUnit unit = PriceUnit.UNIT;
    private Integer packageSize;
    @Singular private List<Discount> applicableDiscounts;
}
