package model.enums;

public enum DiscountType {
    UNITPRICE, PACKAGEPRICE
}
