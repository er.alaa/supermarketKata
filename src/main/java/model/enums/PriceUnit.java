package model.enums;

public enum PriceUnit {
    UNIT, PACKAGE, WEIGHT
}
