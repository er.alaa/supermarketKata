package service;

import model.Discount;
import model.Product;
import model.enums.DiscountType;
import model.enums.PriceUnit;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;


public class PriceCalculatorTest {
    private PriceCalculator priceCalculator;

    @Before
    public void setUp() throws Exception {
        priceCalculator = new PriceCalculator();
    }

    @Test
    public void givenAProduct_whenCalculatePrice_thenPriceShouldBePriceTimesQuantity() {
        final BigDecimal productPrice = BigDecimal.valueOf(44.5);
        final Product product = Product.builder().id(1L).name("Product 1").price(productPrice).build();

        double quantity = 22.3;

        final BigDecimal result = priceCalculator.calculatePrice(product, quantity);
        BigDecimal expectedResult = productPrice.multiply(BigDecimal.valueOf(quantity));

        assertThat(result).isEqualByComparingTo(expectedResult);

    }
    @Test
    public void givenAPackageProduct_whenCalculatePrice_thenPriceShouldBePriceTimesQuantityDividedByPackageSize() {
        final BigDecimal productPrice = BigDecimal.valueOf(44.5);
        int packageSize = 3;
        final Product product = Product.builder().id(1L).name("Product 1").unit(PriceUnit.PACKAGE).packageSize(packageSize).price(productPrice).build();

        double quantity = 9;

        final BigDecimal result = priceCalculator.calculatePrice(product, quantity );
        BigDecimal expectedResult = productPrice.multiply(BigDecimal.valueOf(quantity / packageSize));

        assertThat(result).isEqualByComparingTo(expectedResult);
    }
    @Test
    public void givenAPackageProduct_whenCalculatePriceForAnIncorrectQuantity_thenAnExceptionShouldBeThrown() {
        int packageSize = 3;
        final Product product = Product.builder().id(1L).name("Product 1").unit(PriceUnit.PACKAGE).packageSize(packageSize).price(BigDecimal.valueOf(44.5)).build();

        double quantity = 10;

        Throwable thrown = catchThrowable(() -> priceCalculator.calculatePrice(product, quantity));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);

    }
    @Test
    public void givenADiscountedProduct_whenCalculatePrice_thenDiscountRateShouldBeApplied() {
        final BigDecimal productPrice = BigDecimal.valueOf(44.5);
        final Discount discount = Discount.builder().type(DiscountType.UNITPRICE).discountPourcentage(20).build();
        final Product product = Product.builder().id(1L).name("Product 1").price(productPrice).applicableDiscount(discount).build();

        double quantity = 10;

        final BigDecimal expectedResult = productPrice.multiply(BigDecimal.valueOf(quantity)).multiply(BigDecimal.valueOf(0.8));
        assertThat(priceCalculator.calculatePrice(product, quantity)).isEqualByComparingTo(expectedResult);
    }
    @Test
    public void givenADiscountedPackagePrice_whenCalculatePrice_thenPriceShouldBeAppliedToPackagePlusPriceForExtraItems() {
        final BigDecimal productPrice = BigDecimal.valueOf(44.5);
        final Discount discount = Discount.builder().type(DiscountType.PACKAGEPRICE).discountedPackageSize(5).discountPourcentage(20).build();
        final Product product = Product.builder().id(1L).name("Product 1").price(productPrice).applicableDiscount(discount).build();

        double quantity = 14;

        int numberOfPackages = (int) quantity/discount.getDiscountedPackageSize();
        final BigDecimal packagesPrice = productPrice.multiply(BigDecimal.valueOf(discount.getDiscountedPackageSize())).multiply(BigDecimal.valueOf(numberOfPackages)).multiply(BigDecimal.valueOf(0.8));
        final BigDecimal extraItemsPrice = productPrice.multiply(BigDecimal.valueOf(quantity % discount.getDiscountedPackageSize()));

        final BigDecimal expectedResult = packagesPrice.add(extraItemsPrice);

        assertThat(priceCalculator.calculatePrice(product, quantity)).isEqualByComparingTo(expectedResult);
    }
}